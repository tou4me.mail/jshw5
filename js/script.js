// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
// Создать в объекте вложенный объект - "Приложение".Создать в объекте "Приложение", вложенные
// объекты, "Заголовок, тело, футер, дата".Создать методы для заполнения и отображения
// документа.
var doc = {
  header: '',
  body: '',
  footer: '',
  data: '',
  application: {
    header: {},
    body: {},
    footer: {},
    data: {},
  },

  docFill: function () {
    this.header = 'header' + '<br>';
    this.body = 'body' + '<br>';
    this.footer = 'footer' + '<br>';
    this.data = 'data' + '<br>';
    this.application.header = 'appheader' + '<br>';
    this.application.body = 'appbody' + '<br>';
    this.application.footer = 'appfooter' + '<br>';
    this.application.data = 'appdata' + '<br>';
  },

  docDisp: function () {
    document.write(
      this.header,
      this.body,
      this.footer,
      this.data + '<hr>',
      this.application.header,
      this.application.body,
      this.application.footer,
      this.application.data
    );
  },
};
doc.docFill();
doc.docDisp();
